#!/usr/bin/env python

import pika
import subprocess

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))

channel = connection.channel()

channel.queue_declare(queue='rpc_queue')

def pscount(i):
	if i == "":
		return 0
	elif i == None:
		return 0
	else:
		pslist=subprocess.check_output(['ps','aux'])
		return pslist.count(i)

def on_request(ch, method, props, body):
    n = body

    response = pscount(n)

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = \
                                                         props.correlation_id),
                     body=str(response))
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue='rpc_queue')

channel.start_consuming()
