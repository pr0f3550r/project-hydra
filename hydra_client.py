#!/usr/bin/env python

import os.path
import sys
import pika
import uuid
import configparser

#configfile = Path("/etc/hydra-client.conf")
#if configfile.is_file():
#	print "File exists"
#else:
#	print "Configfile missing!"
#	exit(1)

#config = configparser.RawConfig.Parser()

#processname = "/usr/sbin/dansguardian-av"
#hostname_id = "localhost"
processname = sys.argv[1]
hostname_id = sys.argv[2]


class ProcessCountClient(object):
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host=hostname_id))

        self.channel = self.connection.channel()

        result = self.channel.queue_declare(exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(self.on_response, no_ack=True,
                                   queue=self.callback_queue)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, n):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='',
                                   routing_key='rpc_queue',
                                   properties=pika.BasicProperties(
                                         reply_to = self.callback_queue,
                                         correlation_id = self.corr_id,
                                         ),
                                   body=str(n))
        while self.response is None:
            self.connection.process_data_events()
        return int(self.response)

process_rpc = ProcessCountClient()

response = process_rpc.call(processname)
print(response)

