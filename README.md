Requirements
Two types of system. 
Target system is the system running the Content Filter
Master system is running the PAC file
Must have at least one Target and one Master. Target and Master can be on the same machines or multiple machines.
Install ClearOS 6 or 7 or use existing ClearOS system or systems as ‘Master’
Make sure you are running the ClearOS Content Filter and Web Proxy apps on the ‘Target’ Systems.

Master Node
Set Up Master (The server with the PAC file or write access to the PAC file.)
Configure network access so that the Master can communicate with the Target server(s)
Make sure the Master is up to date with updates


rabbitmq

Install erlang dependency

yum install erlang --enablerepo=epel #CentOS

yum install erlang --enablerepo=clearos-epel #ClearOS

For production, the no-dep erlang package should be used. (https://github.com/rabbitmq/erlang-rpm)


rpm --import https://www.rabbitmq.com/rabbitmq-release-signing-key.asc

yum install rabbitmq-server

Modify rabbitmq server if necessary at:

vi /etc/rabbitmq/rabbitmq.config

Start and Enable rabbitmq

systemctl enable rabbitmq-server

systemctl start rabbitmq-server


Install python-pip

yum install python-pip

Install pika
pip install pika

